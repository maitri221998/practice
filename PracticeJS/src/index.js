import { Book, StoreBook } from './models/BookModel';
import { Category, Store } from './models/CategoryModel';
import { BookUI } from './views/BookView';
import { UI } from './views/CategoryView';
import { login } from './models/Login';
// DOM Load Event
document.addEventListener("DOMContentLoaded", Store.displayCategories);
document.addEventListener("DOMContentLoaded", StoreBook.displayBooks);

//session login
if (sessionStorage.getItem('admin') == null) {
  $('#login').css('display', 'block');
  $('#content').css('display', 'none');
}else{
  $('#login').css('display', 'none');
  $('#content').css('display', 'block');
}
//login form
$('#login-form').submit(function(event) {
  /* Act on the event */
  const user = document.getElementById('user').value;
  const pass = document.getElementById('pass').value;
  if(login(user,pass)){
    $('#login').css('display', 'none');
    $('#content').css('display', 'block');
    sessionStorage.setItem('admin',user);
  }else{
    alert('User or password was wrong!!!')
  }
  event.preventDefault();
});
// Event Listener for add book
document.getElementById("category-form").addEventListener("submit", function(e) {
  // Get form values
  const id = document.getElementById("id").value;
    const name = document.getElementById("name").value;
  // Intantiate category
  const category = new Category(id, name);

  // Intantiate  UI
  const ui = new UI();

  // Validate
  if (id === "" || name === "") {
    // Error alert
    alert("Please fill in all fields");
  } else {
    //add category to list
    if (Store.addCategory(category)) {
      ui.addCategoryToList(category);
      StoreBook.displayBooks();
      alert("Category Added!");
    }else {
      alert('ID already exists');
    }

    // Clear fields
    ui.clearFields();
  }

  e.preventDefault();
});

// Event listener for delete,edit
document.getElementById("category-list").addEventListener("click", function(e) {
  const ui = new UI();
  console.log(e);
  if (e.target.className === 'delete' ) {
    // Delete Book
    ui.deleteCategory(e.target);

    // Remove from LS
    Store.removeCategory(e.target.parentElement.previousElementSibling.previousElementSibling.textContent);
    document.getElementById('category-list').innerHTML='';
    Store.displayCategories();
    StoreBook.displayBooks();
    // Show message
    alert("Category Removed");
  }
  if (e.target.className === 'edit') {
    ui.editCategory(e.target.textContent);
  }
  e.preventDefault();
});
//Submit edit form
 $('#edit-form').submit(function(event) {
   /* Act on the event */
   const id = document.getElementById('idEdit').value;
   const name = document.getElementById('categoryName').value;

   Store.editCategory(id,name);
   document.getElementById('category-list').innerHTML='';
   Store.displayCategories();
   StoreBook.displayBooks();
   alert("Category Edited");
   event.preventDefault();
 });
 //Search category
 $('#searchCategory').keyup(function(event) {
   /* Act on the event */
   let search = document.getElementById('searchCategory').value;
   document.getElementById('category-list').innerHTML='';
   Store.searchCategory(search);
 });


 // ----------------------------BOOK-----------------------------------

document.getElementById("book-form").addEventListener("submit", function(e) {
  // Get form values
  const id = document.getElementById("idBook").value;
    const name = document.getElementById("nameBook").value;
    const desc = document.getElementById("descriptionBook").value;
    const author = document.getElementById("author").value;
    const selectCategory = $("#selectCategory option:selected").val();
  // Intantiate book
  const book = new Book(id, name, desc, author, selectCategory);

  // Intantiate  UI
  const bookUI = new BookUI();

  // Validate
  if (id === "" || name === "" || desc === "" || author === "" ) {
    // Error alert
    alert("Please fill in all fields");
  } else {
    //add Book to list
    if (StoreBook.addBook(book)) {
      bookUI.addBookToList(book);
      document.getElementById('category-list').innerHTML='';
      Store.displayCategories();
      // Show success
      alert("Book Added!");
    }else {
      alert('ID already exists');
    }

    // Clear fields
    bookUI.clearFields();
  }

  e.preventDefault();
});

document.getElementById("book-list").addEventListener("click", function(e) {
  const bookUI = new BookUI();
  console.log(e);
  if (e.target.className === 'delete' ) {
    // Delete Book
    bookUI.deleteBook(e.target);

    // Remove from LS
    StoreBook.removeBook(e.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.textContent);
    document.getElementById('category-list').innerHTML='';
    Store.displayCategories();
    // Show message
    alert("Book Removed");
  }
  if (e.target.className === 'edit') {
  	document.getElementById('selectCategoryEdit').innerHTML = '';
    bookUI.editBook(e.target.textContent);
  }
  e.preventDefault();
});

//Submit edit form
 $('#edit-book-form').submit(function(event) {
   /* Act on the event */
   const id = document.getElementById("idBookEdit").value;
    const name = document.getElementById("bookName").value;
    const desc = document.getElementById("description").value;
    const author = document.getElementById("authorBook").value;
    const selectCategory = $("#selectCategoryEdit option:selected").val();

   StoreBook.editBookFromList(id,name,desc,author,selectCategory);
   document.getElementById('category-list').innerHTML='';
   Store.displayCategories();
   StoreBook.displayBooks();
   alert("Book Edited");
   event.preventDefault();
 });
 //Search book
 $('#searchBook').keyup(function(event) {
  /* Act on the event */
  let search = document.getElementById('searchBook').value;
  document.getElementById('book-list').innerHTML='';
  StoreBook.searchBook(search);
 });
  //Search book by category name
 $('#searchBookByCategory').keyup(function(event) {
  /* Act on the event */
  let search = document.getElementById('searchBookByCategory').value;
  document.getElementById('book-list').innerHTML='';
  StoreBook.searchBookByCategory(search);
 });