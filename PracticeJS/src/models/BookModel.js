import { BookUI } from '../views/BookView';
import { Store } from './CategoryModel';

export class Book {
	constructor(id,name,desc,author,categoryId) {
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.author = author;
		this.categoryId = categoryId;
	}
}

export class StoreBook {
	static getBooks() {
    let books;
    if (localStorage.getItem("books") === null) {
      books = [];
    } else {
      books = JSON.parse(localStorage.getItem("books"));
    }
    return books;
  	}

	static displayBooks() {
		document.getElementById('selectCategory').innerHTML = '';
    	document.getElementById('book-list').innerHTML='';
		const categories = Store.getCategories();

		categories.forEach((category) => {
		  // Todo...
		  const bookUI = new BookUI();

		  bookUI.addCategoryNameToSelect(category);
		});
		const books = StoreBook.getBooks();

		books.forEach((book) => {
		  // Todo...
		  const bookUI = new BookUI();

		  bookUI.addBookToList(book);
		});
	}

	static searchBook(yourSearch) {
	    const books = StoreBook.getBooks();
	    let search = [];

	    if(yourSearch != ''){
	      books.forEach((book) => {
	      // Todo...
	      let testSearch = new RegExp(`^${yourSearch}`,'gi');
	      if(testSearch.test(book.name)){
	        search.push(book);
	      }
	      });
	      const bookUI = new BookUI();
	      search.forEach((s) => {
	        // Todo...
	        bookUI.addBookToList(s);
	      });
	    }else{
	      StoreBook.displayBooks();
	    }
  	}

  	static searchBookByCategory(yoursearch) {
  		const categories = Store.getCategories();
	    const books = StoreBook.getBooks();
	    let searchCate = [];
	    let searchBook = [];

	    if(yoursearch != ''){
	      categories.forEach((cate) => {
		      // Todo...
		      let testSearch = new RegExp(`^${yoursearch}`,'gi');
		      if(testSearch.test(cate.name)){
		        searchCate.push(cate);
		      }
	      });
	      searchCate.forEach((c) => {
	        // Todo...
	        books.forEach((b) => {
	          // Todo...
	          if(c.id == b.categoryId){
	          	searchBook.push(b);
	          }
	        });
	      });
	      const bookUI = new BookUI();
	      searchBook.forEach((s) => {
	        // Todo...
	        bookUI.addBookToList(s);
	      });
	    }else{
	      StoreBook.displayBooks();
	    }
  	}

	static addBook(book) {
	    const books = StoreBook.getBooks();
	    const categories = Store.getCategories();
	    let testIdBook = true;

	    books.forEach((b) => {
	      // Todo...
	      if(book.id == b.id){
	      	testIdBook = false;
	      }
	    });
	    if (testIdBook === true) {
	    	books.push(book);
		    categories.forEach((cate,index) => {
		      // Todo...
		      if (book.categoryId == cate.id) {
		      	categories[index].numberOfBook = categories[index].numberOfBook + 1;
		      }
		    });

		    localStorage.setItem("books", JSON.stringify(books));
		    localStorage.setItem("categories", JSON.stringify(categories));
	    }
	    return testIdBook;
  	}

  	static removeBook(name) {
	    const books = StoreBook.getBooks();
	    const categories = Store.getCategories();
	    let cateID = 0;

	    books.forEach(function(book, index) {
	      if(book.name === name) {
	      	cateID = book.categoryId;
	        books.splice(index, 1);
	      }
	    });
	    categories.forEach((cate,index) => {
	      // Todo...
	      if (cateID == cate.id) {
	      	categories[index].numberOfBook = categories[index].numberOfBook - 1;
	      }
	    })

	    localStorage.setItem("books", JSON.stringify(books));
	    localStorage.setItem("categories", JSON.stringify(categories));
  	}

  	static editBookFromList(id,name,desc,author,categoryId) {
	    const books = StoreBook.getBooks();
	    const categories = Store.getCategories();

	    books.forEach((book,index) => {
	      // Todo...
	      if (book.id == id) {
	        books[index].name = name;
	        books[index].desc = desc;
	        books[index].author = author;
	        if (books[index].categoryId == categoryId) {
	        	books[index].categoryId = categoryId;
	        }else {
	        	categories.forEach((cate,idx) => {
	        	  // Todo...
	        	  if (cate.id == books[index].categoryId) {
	        	  	categories[idx].numberOfBook = categories[idx].numberOfBook - 1;
	        	  }
	        	});
	        	categories.forEach((cate,idx) => {
	        	  // Todo...
	        	  if (cate.id == categoryId) {
	        	  	categories[idx].numberOfBook = categories[idx].numberOfBook + 1;
	        	  }
	        	});
	        	books[index].categoryId = categoryId;
	        }
	      }
	    });

	    localStorage.setItem("books", JSON.stringify(books));
	    localStorage.setItem("categories", JSON.stringify(categories));
  }
}
