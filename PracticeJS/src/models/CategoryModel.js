import { UI } from '../views/CategoryView';

export class Category {
  constructor(id, name) {
    this.id = id;
    this.name = name;
    this.numberOfBook = 0;
  }
}

export class Store {
  static getCategories() {
    let categories;
    if (localStorage.getItem("categories") === null) {
      categories = [];
    } else {
      categories = JSON.parse(localStorage.getItem("categories"));
    }
    return categories;
  }

  static displayCategories() {
    const categories = Store.getCategories();
    const ui = new UI();
    ui.totalOfBook(categories);
    categories.forEach(function(category) {
      // Add book to UI
      ui.addCategoryToList(category);
    });
  }

  static searchCategory(yourSearch) {
    const categories = Store.getCategories();
    let search = [];

    if(yourSearch != ''){
      categories.forEach((cate) => {
      // Todo...
      let testSearch = new RegExp(`^${yourSearch}`,'gi');
      if(testSearch.test(cate.name)){
        search.push(cate);
      }
      });
      const ui = new UI();
      ui.totalOfBook(search);
      search.forEach((s) => {
        // Todo...
        ui.addCategoryToList(s);
      });
    }else{
      Store.displayCategories();
    }
  }

  static addCategory(category) {
    const categories = Store.getCategories();
    let testId = true;

    categories.forEach((cate) => {
      // Todo...
      if(category.id == cate.id){
        testId = false;
      }
    })
    if (testId === true) {
      categories.push(category);
      localStorage.setItem("categories", JSON.stringify(categories));
    }
    return testId;
  }

  static removeCategory(name) {
    const categories = Store.getCategories();

    categories.forEach(function(category, index) {
      if(category.name === name) {
        categories.splice(index, 1);
      }
    });

    localStorage.setItem("categories", JSON.stringify(categories));
  }

  static editCategory(id,name) {
    const categories = Store.getCategories();

    categories.forEach((cate,index) => {
      // Todo...
      if (cate.id == id) {
        categories[index].name = name;
      }
    });

    localStorage.setItem("categories", JSON.stringify(categories));
  }
}