import { UI } from './CategoryView';
import { StoreBook } from '../models/BookModel';
import { Store } from '../models/CategoryModel';

export class BookUI {
	addBookToList(book) {
		const categories = Store.getCategories();
		let nameCategory = '';
		categories.forEach((category) => {
		  // Todo...
		  if (book.categoryId == category.id) {
		  	nameCategory = category.name;
		  }
		});
		const list = document.getElementById("book-list");
	    // Create tr element
	    const row = document.createElement("tr");
	    // Insert cols
	    row.innerHTML = `
	      <td>${book.id}</td>
	      <td><a href="#" class="edit" data-target="#myBookModal" data-toggle="modal">${book.name}</a></td>
	      <td>${book.author}</td>
	      <td>${nameCategory}</td>
	      <td><a href="#" class="delete">X</a></td>
	    `;

	    list.appendChild(row);
	}

	addCategoryNameToSelect(category) {
		const selectCategory = document.getElementById('selectCategory');
		const option = document.createElement('option');
		option.value = category.id;
		option.innerHTML = category.name;

		selectCategory.appendChild(option);
	}

	deleteBook(target) {
	    if (target.className === "delete") {
	      target.parentElement.parentElement.remove();
	    }
  	}

  	editBook(name) {
  		const books = StoreBook.getBooks();
  		let book = null;
  		const categories = Store.getCategories();
  		books.forEach((b) => {
  		  // Todo...
  		  if(b.name === name){
  		  	book = b;
  		  }
  		});
  		document.getElementById('idBookEdit').value = book.id;
  		document.getElementById('bookName').value = book.name;
  		document.getElementById('description').value = book.desc;
  		document.getElementById('authorBook').value = book.author;
  		categories.forEach((category) => {
  		  // Todo...
  		  const selectCategory = document.getElementById('selectCategoryEdit');
			const option = document.createElement('option');
			option.value = category.id;
			option.innerHTML = category.name;

			selectCategory.appendChild(option);
  		});
  	}

	clearFields() {
    document.getElementById("idBook").value = "";
    document.getElementById("nameBook").value = "";
    document.getElementById("descriptionBook").value = "";
    document.getElementById("author").value = "";
  }
}