import { Category, Store } from '../models/CategoryModel';

export class UI {
  addCategoryToList(category) {
    const list = document.getElementById("category-list");
    // Create tr element
    const row = document.createElement("tr");
    // Insert cols
    row.innerHTML = `
      <td>${category.id}</td>
      <td><a href="#" class="edit" data-target="#myModal" data-toggle="modal">${category.name}</a></td>
      <td>${category.numberOfBook}</td>
      <td><a href="#" class="delete">X</a></td>
    `;

    list.appendChild(row);
  }

  deleteCategory(target) {
    if (target.className === "delete") {
      target.parentElement.parentElement.remove();
    }
  }

  editCategory(name) {
    const categories = Store.getCategories();
    let category = null;
    categories.forEach((cate) => {
      // Todo...
      if(cate.name === name){
        category = cate;
      }
    });
    document.getElementById('idEdit').value = category.id;
    document.getElementById('categoryName').value = category.name;
  }

  totalOfBook(categories) {
    let total = 0;

      categories.forEach((cate) => {
      // Todo...
        total = total + cate.numberOfBook;

      });
    document.getElementById('totalOfBook').innerHTML = `Total of book : ${total}`;
  }

  clearFields() {
    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
  }
}